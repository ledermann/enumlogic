require 'active_record'
require 'enumlogic'

# Avoid warning message
I18n.enforce_available_locales = false

ActiveRecord::Base.establish_connection(:adapter => "sqlite3", :database => ":memory:")

ActiveRecord::Schema.verbose = false
ActiveRecord::Schema.define(:version => 1) do
  create_table :computers do |t|
    t.string :kind
  end
end

RSpec.configure do |config|
  config.before(:each) do
    class Computer < ActiveRecord::Base
      private
        def return_false
          false
        end
    end
  end

  config.after(:each) do
    Object.send(:remove_const, :Computer)
  end

  config.expect_with :rspec do |c|
    c.syntax = :should
  end
end
